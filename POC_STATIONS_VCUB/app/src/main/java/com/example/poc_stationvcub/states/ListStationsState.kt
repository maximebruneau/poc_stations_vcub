package com.example.poc_stationvcub.states

import android.location.Location
import com.example.poc_stationvcub.models.Feature

data class ListStationsState(
    val listStations: List<Feature> = emptyList(),
    val userLoc: Location? = null,
    val favStation: Feature? = null,
)