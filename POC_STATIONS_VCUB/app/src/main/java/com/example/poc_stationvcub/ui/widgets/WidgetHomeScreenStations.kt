package com.example.poc_stationvcub.ui.widgets

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.widget.RemoteViews
import com.example.poc_stationvcub.R
import com.example.poc_stationvcub.models.Feature
import com.example.poc_stationvcub.objects.ClientKtor
import com.example.poc_stationvcub.usecases.FetchListStationsStateUseCase
import com.example.poc_stationvcub.usecases.GetFavoriteStationUpdatedUseCase
import com.example.poc_stationvcub.usecases.GetNearestStationUpdatedUseCase
import com.google.android.gms.location.LocationServices
import com.google.gson.Gson
import kotlinx.coroutines.*
import java.text.DateFormat
import java.util.*

/**
 * Implementation of App Widget functionality.
 */
class WidgetHomeScreenStations : AppWidgetProvider(), LocationListener {

    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId, this)
        }
    }


    override fun onLocationChanged(p0: Location) {
    }
}

@SuppressLint("MissingPermission")
internal fun updateAppWidget(
    context: Context,
    appWidgetManager: AppWidgetManager,
    appWidgetId: Int,
    widgetHomeScreenStations: WidgetHomeScreenStations,
) {
    lateinit var usrLoc: Location

    val clientKtor = ClientKtor.createHttpClient()

    var fetchListStationsStateUseCase = FetchListStationsStateUseCase()
    var nearStation: Feature?
    val scope = CoroutineScope(SupervisorJob() + Dispatchers.Default)
    val locationManager =
        context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    val fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
    locationManager.requestLocationUpdates(
        LocationManager.GPS_PROVIDER,
        10000,
        100f,
        widgetHomeScreenStations
    )
    fusedLocationClient.lastLocation
        .addOnSuccessListener { location: Location? ->
            if (location != null) {
                usrLoc = location
                scope.launch(Dispatchers.Main) {
                    val listStationsState = fetchListStationsStateUseCase.invoke(clientKtor)
                    clientKtor.close()
                    val getFavoriteStationUpdatedUseCase = GetFavoriteStationUpdatedUseCase()
                    val getNearestStationUpdatedUseCase = GetNearestStationUpdatedUseCase()
                    var stationFav = getFavStation(context)
                    nearStation =
                        getNearestStationUpdatedUseCase(listStationsState.listStations, usrLoc)
                    val dateString: String = DateFormat.getTimeInstance(DateFormat.SHORT).format(
                        Date()
                    )
                    val views =
                        RemoteViews(context.packageName, R.layout.widget_home_screen_stations)
                    views.setTextViewText(R.id.appwidget_update, (dateString))
                    if (stationFav != null) {
                        getFavoriteStationUpdatedUseCase(listStationsState.listStations, stationFav)
                        views.setTextViewText(R.id.appwidget_fav_name, stationFav.properties.nom)
                        val dispoVeloFav =
                            "Vélos : ${stationFav.properties.nbvelos} , places : ${stationFav.properties.nbplaces}"
                        views.setTextViewText(R.id.appwidget_fav_stats, dispoVeloFav)
                    }

                    if (nearStation != null) {
                        views.setTextViewText(
                            R.id.appwidget_nearest_name,
                            nearStation!!.properties.nom
                        )
                        val dispoVeloNear =
                            "Vélos : ${nearStation!!.properties.nbvelos} , places : ${nearStation!!.properties.nbplaces}"
                        views.setTextViewText(R.id.appwidget_nearest_stats, dispoVeloNear)
                    }
                    val intentUpdate = Intent(context, WidgetHomeScreenStations::class.java)
                    intentUpdate.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
                    val idArray = intArrayOf(appWidgetId)
                    intentUpdate.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, idArray)

                    val pendingUpdate = PendingIntent.getBroadcast(
                        context, appWidgetId, intentUpdate,
                        PendingIntent.FLAG_UPDATE_CURRENT
                    )
                    views.setOnClickPendingIntent(R.id.button_update, pendingUpdate)
                    appWidgetManager.updateAppWidget(appWidgetId, views)
                    scope.cancel()
                }

            }


        }


}

private fun getFavStation(context: Context): Feature? {
    var preferencesFav = context.getSharedPreferences(
        "stations",
        Context.MODE_PRIVATE
    )
    var jsonFav = preferencesFav.getString("stationToAddAsFav", "")
    val stationFav = Gson().fromJson(jsonFav, Feature::class.java)
    return stationFav
}