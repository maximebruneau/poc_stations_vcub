package com.example.poc_stationvcub.ui.home

sealed class HomeEvent {
    object RefreshWidget : HomeEvent()
}