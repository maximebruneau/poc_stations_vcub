package com.example.poc_stationvcub.objects

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.BodyProgress.Feature.install
import io.ktor.client.features.json.*

object ClientKtor {


    val client = HttpClient(CIO) {
        install(JsonFeature)
    }

    fun createHttpClient(): HttpClient {
        return client

    }

}