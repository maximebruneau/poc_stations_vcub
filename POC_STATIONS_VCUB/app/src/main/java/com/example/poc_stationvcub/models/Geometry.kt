package com.example.poc_stationvcub.models

data class Geometry(
    val coordinates: List<Double>,
    val type: String
)