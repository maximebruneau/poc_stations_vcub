package com.example.poc_stationvcub.usecases

import android.location.Location
import com.example.poc_stationvcub.models.Feature

class GetNearestStationUpdatedUseCase {


    operator fun invoke(
        listStations: List<Feature>,
        userLoc: Location? = null
    ): Feature? {

        var nearestStation: Feature? = null
        val myLoc = userLoc
        var distancemin = Double.MAX_VALUE
        
        for (station in listStations) {
            val locToTest: Location? = Location("")
            locToTest!!.longitude = station.geometry.coordinates[0]
            locToTest.latitude = station.geometry.coordinates[1]
            val distanceLoc = myLoc!!.distanceTo(locToTest)
            if (distanceLoc < distancemin) {
                distancemin = distanceLoc.toDouble()
                nearestStation = station
            }
        }
        return nearestStation
    }
}