package com.example.poc_stationvcub.models

import kotlinx.serialization.Serializable

@Serializable
data class jsonStations(
    val features: List<Feature>,
    val type: String
)