package com.example.poc_stationvcub.ui.home

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.poc_stationvcub.states.ListStationsState
import com.example.poc_stationvcub.ui.NavigationFragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices


class HomeFragment : NavigationFragment(), LocationListener {

    private val viewModel: HomeViewModel by viewModels()
    private val homeView: HomeView
        get() = view as HomeView

    private lateinit var locationManager: LocationManager
    private lateinit var usrLoc: Location
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var preferences: SharedPreferences

    override fun buildNavigationView() = HomeView(requireContext())
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        locationManager =
            requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        preferences = requireContext().getSharedPreferences(
            "stations",
            Context.MODE_PRIVATE
        )
        viewModel.setPreferences(preferences)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setInitialLocation()
        homeView.setDetailsInteraction(viewModel::onInteraction)
        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewModel.eventHome.collect {
                when (it) {
                    is HomeEvent.RefreshWidget -> {
                        viewModel.refreshData()
                    }
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenCreated {
            viewModel.stateFlow.collect {

                try {
                    homeView.onStationsState(
                        ListStationsState(
                            it.listStations,
                            usrLoc,
                            it.favStation
                        )
                    )

                } catch (e: Exception) {
                    homeView.onStationsState(
                        ListStationsState(
                            it.listStations,
                            null,
                            it.favStation
                        )
                    )
                }
            }
        }
        viewModel.refreshData()
    }

    @SuppressLint("MissingPermission")
    fun setInitialLocation() {
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 100f, this)
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                if (location != null) {
                    usrLoc = location
                    setLocation()
                }
            }
    }

    fun setLocation() {
        viewModel.setLoc(usrLoc)
    }

    override fun onLocationChanged(p0: Location) {
        usrLoc = p0
        setLocation()
        viewModel.refreshData()
    }

}