package com.example.poc_stationvcub.usecases

import com.example.poc_stationvcub.models.Feature

class GetFavoriteStationUpdatedUseCase {


    operator fun invoke(
        listStations: List<Feature>,
        favoriteStation: Feature
    ): Feature? {
        var favoriteStationUpdated: Feature? = null
        favoriteStationUpdated = listStations
            .filter {
                (it.properties.ident == favoriteStation.properties.ident)
            }[0]

        return favoriteStationUpdated
    }
}
