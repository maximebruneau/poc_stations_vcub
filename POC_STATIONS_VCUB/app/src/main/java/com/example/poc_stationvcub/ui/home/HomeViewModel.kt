package com.example.poc_stationvcub.ui.home

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.location.Location
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.poc_stationvcub.models.Feature
import com.example.poc_stationvcub.objects.ClientKtor
import com.example.poc_stationvcub.states.ListStationsState
import com.example.poc_stationvcub.usecases.FetchListStationsStateUseCase
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlin.coroutines.EmptyCoroutineContext


class HomeViewModel : ViewModel() {

    private lateinit var preferences: SharedPreferences
    private val clientKtor = ClientKtor.createHttpClient()

    private val mutableDetailsEvent = MutableSharedFlow<HomeEvent>()
    val eventHome: SharedFlow<HomeEvent> = mutableDetailsEvent.asSharedFlow()
    private val fetchListStationsUseCase = FetchListStationsStateUseCase()
    private val mutableState by lazy {
        runBlocking(EmptyCoroutineContext) {
            MutableStateFlow(ListStationsState())
        }
    }
    val stateFlow: StateFlow<ListStationsState> by lazy { mutableState.asStateFlow() }
    var userLocation: Location? = null

    @SuppressLint("StaticFieldLeak")

    fun onInteraction(interaction: HomeInteraction) {

        when (interaction) {
            is HomeInteraction.OnRefreshMap -> {
                refreshData()
            }

            is HomeInteraction.OnAddToFavorite -> {

                val editeur = preferences.edit()
                val gson = Gson()
                val json = gson.toJson(interaction.stationToAddAsFav)
                editeur.putString("stationToAddAsFav", json)
                editeur.apply()
            }
        }
    }

    fun refreshData() {
        viewModelScope.launch(Dispatchers.IO)
        {
            val fetchListStationsUseCase = FetchListStationsStateUseCase()

            val json = preferences.getString("stationToAddAsFav", "")
            val stationFav = Gson().fromJson(json, Feature::class.java)
            //superflu mais sinon ne trigger pas le state car détecte pas de changement, à revoir
            var listStationsToSend = emptyList<Feature>().toMutableList()
            val listStationsState = fetchListStationsUseCase.invoke(clientKtor, stationFav)
//            var listStationsNew= listStationsState.listStations as MutableList<Feature>
//            listStationsToSend.addAll(listStationsNew)
//            mutableState.emit(ListStationsState(listStationsToSend , favStation = listStationsState.favStation ))
            mutableState.emit(listStationsState)
        }
    }

    fun setLoc(usrLoc: Location) {
        userLocation = usrLoc
    }

    fun setPreferences(preferences: SharedPreferences?) {

        if (preferences != null) {
            this.preferences = preferences
        }
    }
}