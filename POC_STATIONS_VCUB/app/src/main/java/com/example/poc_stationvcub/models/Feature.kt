package com.example.poc_stationvcub.models

import kotlinx.serialization.Serializable


@Serializable
data class Feature(
    val geometry: Geometry,
    val properties: Properties,
    val type: String
)