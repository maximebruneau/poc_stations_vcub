package com.example.poc_stationvcub.models

data class Properties(
    val cdate: String,
    val etat: String,
    val geom_err: Any,
    val geom_o: Int,
    val gid: Int,
    val ident: Int,
    val mdate: String,
    val nbclassiq: Int,
    val nbelec: Int,
    val nbplaces: Int,
    val nbvelos: Int,
    val nom: String,
    val type: String
)