package com.example.poc_stationvcub.ui.home

import com.example.poc_stationvcub.models.Feature


sealed class HomeInteraction {

    object OnRefreshMap : HomeInteraction()
    data class OnAddToFavorite(val stationToAddAsFav: Feature?) : HomeInteraction()


}