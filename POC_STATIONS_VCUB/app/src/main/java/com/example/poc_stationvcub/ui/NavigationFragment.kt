package com.example.poc_stationvcub.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment

abstract class NavigationFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        buildNavigationView().also { view ->
            // generate an id to
            view.id = javaClass.hashCode()

            val layoutParams = ConstraintLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            view.layoutParams = layoutParams
        }

    /**
     * Get view related to this navigation fragment
     */
    protected abstract fun buildNavigationView(): View

    /**
     * Get mode of status bar to know if texts and icons
     * of status bar are light or dark
     */


}