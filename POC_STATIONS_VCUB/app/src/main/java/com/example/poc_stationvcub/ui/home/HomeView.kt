package com.example.poc_stationvcub.ui.home

import MyItem
import android.content.Context
import android.location.Location
import android.view.LayoutInflater
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import com.example.poc_stationvcub.databinding.HomeViewBinding
import com.example.poc_stationvcub.models.Feature
import com.example.poc_stationvcub.states.ListStationsState
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterManager

class HomeView(context: Context) : OnMapReadyCallback, GoogleMap.OnMapClickListener,
    GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMarkerClickListener,
    ConstraintLayout(context) {

    private var binding: HomeViewBinding
    private lateinit var mGoogleMap: GoogleMap
    private var mapView: MapView
    private var onGoing: Boolean = false
    lateinit var onInteraction: (HomeInteraction) -> Unit
    private var hash = mutableMapOf<LatLng, Feature>()
    private lateinit var clusterManager: ClusterManager<MyItem>
    private var stationToAddAsFav: Feature? = null

    init {
        val layoutInflater = LayoutInflater.from(context)
        binding = HomeViewBinding.inflate(layoutInflater, this)
        mapView = binding.mapView

        if (mapView != null) {
            mapView.onCreate(null)
            mapView.onResume()
            mapView.getMapAsync(this)
        }
        binding.buttonRefresh.setOnClickListener {
            callRefresh()
            val toast = Toast.makeText(context, "Les données sont à jour.", Toast.LENGTH_LONG)
            toast.show()
        }
        binding.buttonAddToFavorite.setOnClickListener {

            if (stationToAddAsFav != null) {
                onInteraction(
                    HomeInteraction.OnAddToFavorite(stationToAddAsFav)
                )
                callRefresh()
            }
            binding.buttonAddToFavorite.isVisible = false

        }
    }

    override fun onMapReady(p0: GoogleMap) {
        context?.let { MapsInitializer.initialize(it) }
        mGoogleMap = p0
        mGoogleMap.mapType = GoogleMap.MAP_TYPE_SATELLITE
        mGoogleMap.setOnInfoWindowClickListener(this)
        clusterManager = ClusterManager(context, mGoogleMap)
        mGoogleMap.setOnMarkerClickListener(this)
        mGoogleMap.setOnMapClickListener(this)
    }

    fun setDetailsInteraction(interaction: (HomeInteraction) -> Unit) {
        onInteraction = interaction
    }

    private fun callRefresh() {
        onInteraction(
            HomeInteraction.OnRefreshMap
        )
    }

    fun displayUserLoc(usrLoc: Location) {
        mGoogleMap.clear()
        mGoogleMap.addMarker(
            MarkerOptions().position(
                LatLng(
                    usrLoc.latitude,
                    usrLoc.longitude
                )
            ).title("JE SUIS ICI").icon(
                BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN)
            )
        )
        if (!onGoing) {
            mGoogleMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(
                        usrLoc.latitude,
                        usrLoc.longitude
                    ), 14f
                )
            )

            onGoing = true
        }
    }

    fun onStationsState(state: ListStationsState) {
        if (state.userLoc != null) {
            displayUserLoc(state.userLoc)
        }
        try {
            hash.clear()
            clusterManager.clearItems()
        } catch (e: Exception) {
        }
        var marker: MarkerOptions?

        for (station in state.listStations) {

            var latlng = LatLng(
                station.geometry.coordinates[1],
                station.geometry.coordinates[0]
            )
            marker = MarkerOptions().position(
                latlng
            ).title(station.properties.nom).snippet(
                "Velos dispos : ${station.properties.nbvelos} " +
                        ", places dispos : ${station.properties.nbplaces}"
            )
            var markos = mGoogleMap.addMarker(marker)

            if (station == state.favStation) {
                markos!!.remove()
            }
            hash.put(marker.position, station)
        }
        if (state.favStation != null) {
            val latlng = LatLng(
                state.favStation.geometry.coordinates[1],
                state.favStation.geometry.coordinates[0]
            )

            marker = MarkerOptions().position(
                latlng
            ).title(state.favStation.properties.nom).snippet(
                "Velos dispos : " +
                        "${state.favStation.properties.nbvelos} " +
                        ", places dispos : ${state.favStation.properties.nbplaces}"
            ).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW))
            mGoogleMap.addMarker(marker)
            hash.put(marker.position, state.favStation)
        }
    }

    override fun onInfoWindowClick(p0: Marker) {

        try {
            val station = hash[p0.position]!!

            binding.buttonAddToFavorite.text = "Mettre ${station.properties.nom} en favori "

            val toast = Toast.makeText(
                context, "DISPO: " +
                        "${station.properties.nbclassiq} classiques, ${station.properties.nbelec} " +
                        "électriques", Toast.LENGTH_LONG
            )
            toast.show()
        } catch (e: Exception) {
            val toast = Toast.makeText(context, "C'est vous", Toast.LENGTH_LONG)
            toast.show()
        }
    }

    override fun onMarkerClick(p0: Marker): Boolean {
        val station = hash[p0.position]!!
        stationToAddAsFav = station
        binding.buttonAddToFavorite.isVisible = true
        binding.buttonAddToFavorite.text = "Mettre ${station.properties.nom} en favori "
        return false
    }

    override fun onMapClick(p0: LatLng) {
        binding.buttonAddToFavorite.isVisible = false
    }
}