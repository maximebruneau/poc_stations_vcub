package com.example.poc_stationvcub.usecases

import android.location.Location
import com.example.poc_stationvcub.models.Feature
import com.example.poc_stationvcub.models.jsonStations
import com.example.poc_stationvcub.states.ListStationsState
import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.http.*

class FetchListStationsStateUseCase {

    var listStations = mutableListOf<Feature>()
    suspend operator fun invoke(
        clientKtor: HttpClient,
        stationFav: Feature? = null,
        userLocation: Location? = null
    ): ListStationsState {

        val jsonstations: jsonStations = clientKtor.get(
            "https://data.bordeaux-metropole.fr/geojson?key=39CDFINPUU&typename=ci_vcub_p"
        )
        {
            contentType(ContentType.Application.Json)
        }
        listStations.clear()

        for (station in jsonstations.features) {
            listStations.add(station)
        }
        setList(listStations)

        return ListStationsState(
            listStations,
            favStation = stationFav,
            userLoc = userLocation,
        )
    }

    private fun setList(newList: List<Feature>) {
        listStations = newList as MutableList<Feature>
    }

}
